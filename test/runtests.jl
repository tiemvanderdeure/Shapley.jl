using Shapley, Test
using Random, Tables, Distributions, DataFrames, ComputationalResources
using Shapley: ⊟

const S = Shapley

# DataFrames is a test dependency since it is so commonly used, but we make it easy to turn off the tests for it
const DATAFRAMES = true


include("Toys.jl")
import .Toys


@testset "Shapley" begin

@testset "utils" begin
    X = (A=rand(3), B=rand(3))
    @test S.featureindex(X, 1) == 1
    @test S.featureindex(X, 2) == 2
    @test S.featureindex(X, :A) == 1
    @test S.featureindex(X, :B) == 2

    @test_throws ArgumentError S.compatibletable(1)
    X = (A=rand(3), B=rand(3))
    @test S.compatibletable(X) isa Tables.Columns
    X = [1 2; 3 4]
    @test S.compatibletable(X) isa Tables.Columns

    x = ones(Int, 4)
    @test x ⊟ x == zeros(Int, 4)

    f = DiscreteUniform(1, 3)
    o = [f] ⊟ [f]
    @test all(∈([Symbol("1"), Symbol("2"), Symbol("3")]), keys(o))
    @test all(x -> only(x) ≈ 0.0, values(o))

    X = (A=rand(3), B=rand(Int, 3), C=["" for i ∈ 1:3])
    X′ = S.similartable(X)
    @test keys(X′) == keys(X)
    @test eltype(Tables.getcolumn(X′, :A)) == eltype(Tables.getcolumn(X, :A))
    @test eltype(Tables.getcolumn(X′, 2)) == eltype(Tables.getcolumn(X, 2))
    @test eltype(Tables.getcolumn(X′, :C)) == eltype(Tables.getcolumn(X, :C))

    X = (A=ones(Int, 3), B=ones(Int, 3))
    @test S.tableop(+, X, X) == (A=2ones(Int,3), B=2ones(Int,3))
    @test S.tableop(*, X, 0) == (A=zeros(Int,3), B=zeros(Int,3))

    v = [4,5,6]
    w = S.ntcopy(v)
    w .= [1,2,3]
    @test v == [4,5,6]
    v = (A=[4,5,6],)
    w = S.ntcopy(v)
    w[:A] .= [1,2,3]
    @test v == (A=[4,5,6],)

    z = S.coalitions(3, 1, 1)
    zr = collect(eachrow(z))
    @test [1,0,0] ∈ zr
    @test [0,1,0] ∈ zr
    @test [0,0,1] ∈ zr
    @test [0,1,1] ∈ zr
    @test [1,0,1] ∈ zr
    @test [1,1,0] ∈ zr

    z = S.coalitions(5, 1, 2)
    zr = collect(eachrow(z))
    @test [1, 0, 0, 1, 1] ∈ zr
    @test [0, 0, 0, 1, 0] ∈ zr
    @test [1, 1, 1, 1, 0] ∈ zr
    @test [0, 1, 0, 1, 0] ∈ zr

    x = zeros(Int, 10, 4)
    x[1, :] .= 1
    ξ = Tables.Columns(Tables.table(x))
    S.h!(ξ, Random.GLOBAL_RNG, Bool[1,0,0,0])
    @test ξ[1][1] == 1
    @test ξ[1][2] == 0
    @test ξ[1][10] == 0

    DATAFRAMES && @testset "dataframes" begin
        X = DataFrame(A=ones(Int, 4), B=ones(Int, 4))
        @test S.compatibletable(X) isa Tables.Columns
        @test S.tableop(+, X, X) == (A=2ones(Int,4), B=2ones(Int,4))
        @test S.tableop(*, X, 0) == (A=zeros(Int,4), B=zeros(Int,4))
    end
end

@testset "montecarlo" begin
    rng = MersenneTwister(999)

    # imputeshuffled!
    ξ = (A=(-1)*ones(3), B=(-1)*ones(3))
    X = (A=zeros(3), B=zeros(3))
    Z = (A=ones(3), B=ones(3))
    p = S.imputeshuffled!(rng, 1, S.compatibletable(ξ), S.compatibletable(X), S.compatibletable(Z))
    @test all(≈(-1.0), ξ[:A])
    @test all(x -> x ≈ 0.0 || x ≈ 1.0, ξ[:B])
    @test all(∈(1:3), p)

    # shuffledata
    X = (A=(-1)*ones(Int, 4), B=(-1)*ones(Int, 4))
    Z = (A=ones(Int, 2), B=ones(Int, 2))
    ξ, ζ = S.shuffledata(rng, X, 1, Z)
    @test ξ[:A] == Z[:A]
    @test all(∈(X[:A]), ζ[:A])
    @test ξ[:B] == ζ[:B]
    @test all(∈((-1,1)), ξ[:B])

    DATAFRAMES && @testset "dataframes" begin
        # imputeshuffled!
        ξ = DataFrame(A=(-1)*ones(3), B=(-1)*ones(3))
        X = DataFrame(A=zeros(3), B=zeros(3))
        Z = DataFrame(A=ones(3), B=ones(3))
        p = S.imputeshuffled!(rng, 1, S.compatibletable(ξ), S.compatibletable(X), S.compatibletable(Z))
        @test all(≈(-1.0), ξ[:, :A])
        @test all(x -> x ≈ 0.0 || x ≈ 1.0, ξ[:, :B])
        @test all(∈(1:3), p)

        # shuffledata
        X = DataFrame(A=(-1)*ones(Int, 4), B=(-1)*ones(Int, 4))
        Z = DataFrame(A=ones(Int, 2), B=ones(Int, 2))
        ξ, ζ = S.shuffledata(rng, X, 1, Z)
        @test Tables.getcolumn(ξ, :A) == Z[:, :A]
        @test all(∈(X[:, :A]), Tables.getcolumn(ζ, :A))
        @test Tables.getcolumn(ξ, :B) == Tables.getcolumn(ζ, :B)
        @test all(∈((-1,1)), Tables.getcolumn(ξ, :B))
    end
end

function sum_rule_linear(rng, algo)
    f = Toys.linear([1.0, 1.0, 1.0], 0.0)
    X, y = Toys.data(f, 1024, 3, 0.05; rng)
    rhs = f(X) .- mean(f(X))
    ϕ = DataFrame(shapley(f, algo, X))  # convenient way to get a standard form
    ϕ = collect(eachcol(ϕ))
    # the variance can be large, so we are extremely generous with the bounds here
    # we basically need the difference to be ≪ 1 for the above example
    @test all(-0.1 .< sum(ϕ) - rhs .< 0.1)
end

function sum_rule_quadratic(rng, algo)
    f = Toys.quadratic([1.0 0.0 0.0; 0.0 1.0 0.0; 0.0 0.0 1.0], zeros(3), 0.0)
    X, y = Toys.data(f, 1024, 3, 0.05; rng)
    rhs = f(X) .- mean(f(X))
    ϕ = DataFrame(shapley(f, algo, X))
    ϕ = collect(eachcol(ϕ))
    @test all(-0.1 .< sum(ϕ) - rhs .< 0.1)
end

function null_condition_linear(rng, algo)
    f = Toys.linear([1.0, 1.0, 0.0], 0.0)
    X, y = Toys.data(f, 1024, 3, 0.05; rng)
    ϕ = Tables.columntable(shapley(f, algo, X))[:Column3]
    @test all(-0.1 .≤ ϕ .≤ 0.1)
end

function null_condition_quadratic(rng, algo)
    f = Toys.quadratic([1.0 0.0 0.0; 0.0 1.0 0.0; 0.0 0.0 0.0], zeros(3), 0.0)
    X, y = Toys.data(f, 1024, 3, 0.05; rng)
    ϕ = Tables.columntable(shapley(f, algo, X))[:Column3]
    @test all(-0.1 .≤ ϕ .≤ 0.1)
end

@testset "properties" begin
    @testset "sum_rule" begin
        rng = MersenneTwister(999)

        sum_rule_linear(rng, Shapley.MonteCarlo(1024))
        sum_rule_linear(rng, Shapley.MonteCarlo(CPUThreads(), 1024))

        sum_rule_linear(rng, Shapley.KernelSHAP(CPUThreads(), 1, 2048))  # need a lot of iterations for this to pass

        sum_rule_quadratic(rng, Shapley.MonteCarlo(1024))
        sum_rule_quadratic(rng, Shapley.MonteCarlo(CPUThreads(), 1024))

        sum_rule_quadratic(rng, Shapley.KernelSHAP(CPUThreads(), 1, 2048))
    end

    @testset "null_condition" begin
        rng = MersenneTwister(999)

        null_condition_linear(rng, Shapley.MonteCarlo(1024))
        null_condition_linear(rng, Shapley.MonteCarlo(CPUThreads(), 1024))

        null_condition_linear(rng, Shapley.KernelSHAP(CPUThreads(), 1, 2048))

        null_condition_quadratic(rng, Shapley.MonteCarlo(1024))
        null_condition_quadratic(rng, Shapley.MonteCarlo(CPUThreads(), 1024))

        null_condition_quadratic(rng, Shapley.KernelSHAP(CPUThreads(), 1, 2048))
    end
end

end
