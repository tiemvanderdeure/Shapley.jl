# [Bibliography](@id bibliography)

1. Štrumbelj, Erik, and Igor Kononenko. "Explaining prediction models and individual predictions with feature contributions." Knowledge and information systems 41.3 (2014): 647-665
2. Kjersti Aas, Martin Jullum and Anders Løland. Explaining individual predictions when features are dependent: More accurate approximations to Shapley values [arXiv:1903.10464](https://arxiv.org/abs/1903.10464)
3. Lundberg, Scott and Lee, Su-In. "A Unified Approach to Interpreting Model Predictions" [arXiv:1705.07874](https://arxiv.org/abs/1705.07874)
4. Lundberg, Scott and Erion, Gabriel and Lee, Su-In.  "Consistent Individualized Feature
   Attribution for Tree Ensembles" [arXiv:1802.03888](https://arxiv.org/abs/1802.03888)


# Additional Resources

- [Interpretable Machine Learning](https://christophm.github.io/interpretable-ml-book/shapley.html), an HTML book by [Christopher Molnar](https://github.com/christophM).
- [`shap`](https://github.com/slundberg/shap) is a Python package for computing Shapley
    values and some similar feature importance values using a wide variety of methods.
